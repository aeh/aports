# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=expat
pkgver=2.4.3
pkgrel=0
pkgdesc="XML Parser library written in C"
url="http://www.libexpat.org/"
arch="all"
license='MIT'
checkdepends="bash"
source="https://downloads.sourceforge.net/project/expat/expat/$pkgver/expat-$pkgver.tar.bz2"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"

# secfixes:
#   2.4.3-r0:
#     - CVE-2021-45960
#     - CVE-2021-46143
#     - CVE-2022-22822
#     - CVE-2022-22823
#     - CVE-2022-22824
#     - CVE-2022-22825
#     - CVE-2022-22826
#     - CVE-2022-22827
#   2.2.7-r1:
#     - CVE-2019-15903
#   2.2.7-r0:
#     - CVE-2018-20843
#   2.2.0-r1:
#     - CVE-2017-9233

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--enable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir/" install
}

sha512sums="
1a77580c10d8cd1eb2c9224697cb73cdad742c1b6cf716d987379d01bb1f66240c315c298f5295f120cf44445521ccb7cdd39db1e743f164b919245a35a9468e  expat-2.4.3.tar.bz2
"
