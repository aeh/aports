# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=fd
pkgver=8.3.1
pkgrel=0
pkgdesc="Simple, fast, user-friendly alternative to find"
url="https://github.com/sharkdp/fd"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="MIT Apache-2.0"
makedepends="cargo"
checkdepends="coreutils"
options="net"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/sharkdp/fd/archive/v$pkgver.tar.gz
	minimize-size.patch
	"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	cargo install --frozen --offline --path . --root="$pkgdir"/usr
	rm "$pkgdir"/usr/.crates*

	install -Dm644 doc/fd.1 "$pkgdir"/usr/share/man/man1/fd.1

	find "$builddir"/target/release/build/fd-find-* -type f -iname fd.bash \
		-exec install -Dm644 '{}' "$pkgdir"/usr/share/bash-completion/completions/fd \;
	find "$builddir"/target/release/build/fd-find-* -type f -iname fd.fish \
		-exec install -Dm644 '{}' "$pkgdir"/usr/share/fish/completions/fd.fish \;
	install -Dm644 "$builddir"/contrib/completion/_fd "$pkgdir"/usr/share/zsh/site-functions/_fd
}

sha512sums="
1f37ed817629f580986c7534636dc2cac6658177dda4717e86bfa29c6122aae52e562c0a026c287687053e11430087578a9ca9a166997bfe99417490530d03fb  fd-8.3.1.tar.gz
5f5f1dff80a31cbd532ca220ea399436924fedeef418f84bf6bf715a76ac8c456093fe6f8409f182fe5c94741dd7916d85b4fe3b7106e287a481ab863672ed3b  minimize-size.patch
"
