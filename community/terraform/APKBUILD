# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.1.4
pkgrel=1
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
checkdepends="openssh-client"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/v$pkgver.tar.gz"
builddir="$srcdir/src/github.com/hashicorp/$pkgname"
options="chmod-clean"

prepare() {
	mkdir -p ${builddir%/*}
	mv $srcdir/$pkgname-$pkgver "$builddir"/
	default_prepare
}

export GOPATH="$srcdir"

build() {
	go build -v -o bin/$pkgname \
		-mod=readonly -ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease= -s -w"
}

check() {
	case "$CARCH" in
		arm*|x86)
			go list -mod=readonly . | xargs -t -n4 \
			go test -mod=readonly -timeout=2m -parallel=4
			;;
		*) go test ./... ;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
de1768d3d5134f1d34ec59365fd18a27fdf94a4ab1f9f3aa2996368596aa79c7d16eef82d62c91f544bd95630aac8a5f3dc3f03d784c1bacd9cd4aff9b3496a9  terraform-1.1.4.tar.gz
"
