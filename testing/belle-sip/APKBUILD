# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=belle-sip
pkgver=5.0.66
pkgrel=0
pkgdesc="SIP (RFC3261) implementation written in C"
url="https://www.linphone.org"
arch="all !mips !mips64 !riscv64" # java
license="GPL-2.0-or-later"
options="!check" # no test available
makedepends="cmake libantlr3c libantlr3c-dev bctoolbox-dev
zlib-dev mbedtls-dev java-jre-headless belr-dev"
subpackages="$pkgname-dev"
source="https://gitlab.linphone.org/BC/public/belle-sip/-/archive/$pkgver/belle-sip-$pkgver.tar.gz
	antlr.jar::https://github.com/antlr/website-antlr3/blob/gh-pages/download/antlr-3.4-complete.jar?raw=true"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_MODULE_PATH=/usr/lib/cmake \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DENABLE_STATIC=NO \
		-DENABLE_SHARED=YES \
		-DENABLE_TESTS=NO \
		-DENABLE_STRICT=NO \
		-DANTLR3_JAR_PATH="$srcdir"/antlr.jar
	make -C build
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="
9ad4cef3790e1324f017e8c21eaca1d84f0f8f6ebca9870d648b78a46830f79cc9a33dc6648e082d0a5f642b369767787996e25283736527524b6f215fc2f61f  belle-sip-5.0.66.tar.gz
04be4dfba3a21f3ab9d9e439a64958bd8e844a9f151b798383bd9e0dd6ebc416783ae7cb1d1dbb27fb7288ab9756b13b8338cdb8ceb41a10949c852ad45ab1f2  antlr.jar
"
