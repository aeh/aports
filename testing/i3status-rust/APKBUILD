# Contributor: Galen Abell <galen@galenabell.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=i3status-rust
pkgver=0.21.2
pkgrel=0
pkgdesc="i3status replacement in Rust"
url="https://github.com/greshake/i3status-rust"
arch="all !s390x !mips !mips64 !riscv64" # limited by cargo
license="GPL-3.0-only"
makedepends="rust cargo dbus-dev openssl-dev libpulse"
options="net !check" # no tests
provides="i3status-rs"
subpackages="$pkgname-doc"
source="https://github.com/greshake/i3status-rust/archive/v$pkgver/i3status-rust-$pkgver.tar.gz"

build() {
	cargo build --release --verbose --locked
}

package() {
	install -Dm755 target/release/i3status-rs "$pkgdir"/usr/bin/i3status-rs

	install -Dm644 man/i3status-rs.1 "$pkgdir"/usr/share/man/man1/i3status-rs.1

	install -Dm644 -t "$pkgdir"/usr/share/i3status-rust/themes files/themes/*
	install -Dm644 -t "$pkgdir"/usr/share/i3status-rust/icons files/icons/*
}

sha512sums="
d3b916e8bb24d744cffc8987ca9bc0e9a2b3e9eb6c3a32f158b6e91ea48068247b564f0230217fedb4b8aa9a8c82e39cb8495f8875a065f4863954fc94105ff5  i3status-rust-0.21.2.tar.gz
"
